@echo off
Setlocal EnableExtensions EnableDelayedExpansion
chcp 1251

set inifile=asadmin.properties

SetLocal
call :ini.ReadKey "%inifile%" "[settings]" 1 gf_home 1
endlocal&set GF_HOME=%ini.Value%
echo GF_HOME=%GF_HOME%

SetLocal
call :ini.ReadKey "%inifile%" "[settings]" 1 asadmin_folder 1
endlocal&set ASADMIN_FOLDER=%ini.Value%
echo ASADMIN_FOLDER=%ASADMIN_FOLDER%

SetLocal
call :ini.ReadKey "%inifile%" "[settings]" 1 address 1
endlocal&set ADDRESS=%ini.Value%
echo ADDRESS=%ADDRESS%

SetLocal
call :ini.ReadKey "%inifile%" "[settings]" 1 port 1
endlocal&set PORT=%ini.Value%
echo PORT=%PORT%

SetLocal
call :ini.ReadKey "%inifile%" "[javamail]" 1 mail_jndi 1
endlocal&set MAIL_JNDI=%ini.Value%
echo MAIL_JNDI=%MAIL_JNDI%

SetLocal
call :ini.ReadKey "%inifile%" "[javamail]" 1 mailhost 1
endlocal&set MAILHOST=%ini.Value%
echo MAILHOST=%MAILHOST%

SetLocal
call :ini.ReadKey "%inifile%" "[javamail]" 1 mailuser 1
endlocal&set MAILUSER=%ini.Value%
echo MAILUSER=%MAILUSER%

SetLocal
call :ini.ReadKey "%inifile%" "[javamail]" 1 fromaddress 1
endlocal&set FROMADDRESS=%ini.Value%
echo FROMADDRESS=%FROMADDRESS%

SetLocal
call :ini.ReadKey "%inifile%" "[javamail]" 1 description1 1
endlocal&set DESCRIPTION=%ini.Value%
echo DESCRIPTION=%DESCRIPTION%

SetLocal
call :ini.ReadKey "%inifile%" "[javamail]" 1 storeprotocol 1
endlocal&set STOREPROTOCOL=%ini.Value%
echo STOREPROTOCOL=%STOREPROTOCOL%

SetLocal
call :ini.ReadKey "%inifile%" "[javamail]" 1 storeprotocolclass 1
endlocal&set STOREPROTOCOLCLASS=%ini.Value%
echo STOREPROTOCOLCLASS=%STOREPROTOCOLCLASS%

SetLocal
call :ini.ReadKey "%inifile%" "[javamail]" 1 transprotocol 1
endlocal&set TRANSPROTOCOL=%ini.Value%
echo TRANSPROTOCOL=%TRANSPROTOCOL%

SetLocal
call :ini.ReadKey "%inifile%" "[javamail]" 1 transprotocolclass 1
endlocal&set TRANSPROTOCOLCLASS=%ini.Value%
echo TRANSPROTOCOLCLASS=%TRANSPROTOCOLCLASS%

SetLocal
call :ini.ReadKey "%inifile%" "[javamail]" 1 properties 1
endlocal&set PROPERTIES=%ini.Value%
echo PROPERTIES=%PROPERTIES%

if "%MAIL_JNDI%" NEQ "" (
    %GF_HOME%\%ASADMIN_FOLDER%\asadmin.bat -H %ADDRESS% -p %PORT% -u admin  create-javamail-resource --mailhost %MAILHOST% --mailuser %MAILUSER% --fromaddress %FROMADDRESS% --description "%DESCRIPTION%" --storeprotocol %STOREPROTOCOL% --storeprotocolclass %STOREPROTOCOLCLASS% --transprotocol %TRANSPROTOCOL% --transprotocolclass %TRANSPROTOCOLCLASS% --property %PROPERTIES% %MAIL_JNDI%
)

pause


:ini.ReadKey
::%1 - ���� � �����
::%2 - ��� ������
::%3 - ���������� ����� ������ (���� ������� �����������), ����� ���������� 1
::%4 - ��� ���������
::%5 - ���������� ����� ��������� (���� ������� �����������), ����� ���������� 1
  for /f "UseBackQ delims=" %%s in ("%~1") do (
    if Defined BeginRead (
      for /f "tokens=1* delims==" %%k in ("%%s") do (
        set ini.Key.Name=%%k
        call :TrimSpaces "!ini.Key.Name!" " " "ini.Key.Name"
        if /i "!ini.Key.Name!"=="%~4" (
          Set /A ini.Keys.Count+=1
          if !ini.Keys.Count!==%~5 (
            Set ini.Value=%%l
            call :TrimSpaces "!ini.Value!" " " "ini.Value"
            Exit /B
          )
        )
      )
    ) else (
      set ini.Section.Name=%%s
      call :TrimSpaces "!ini.Section.Name!" " " "ini.Section.Name"
      if /i "!ini.Section.Name!"=="%~2" (
        set /A ini.Sections.Count+=1
        if !ini.Sections.Count!==%~3 (
          Set BeginRead=true
        )
      )
    )
  )
Exit /B
 
::������� ����� � ������ ������ �������, ��� ������ ������ ������ �������, �������� ���������� %2 (Symbol)
:TrimSpaces
::%1-in.String
::%2-in.Symbol
::%3-out.String.Variable
  Set "%~3="
  Set "_String=%~1"
  if "%_String%"=="" Exit /B
  :Begin_Trim_Left
    if "%_String:~0,1%"=="%~2" (set "_String=%_String:~1%"& Goto Begin_Trim_Left)
  if "%_String%"=="" Exit /B
  :Begin_Trim_Right  
    if "%_String:~-1%"=="%~2" (set "_String=%_String:~0,-1%"& Goto Begin_Trim_Right)
  set "%~3=%_String%"
Exit /B

