package ru.apertum.qsystem.prereg;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import ru.apertum.qsystem.prereg.core.ServiceList;
import ru.apertum.qsystem.server.model.QService;

import java.util.LinkedList;

public class ServiceForm {

    private Logger log = LogManager.getLogger(ServiceForm.class);

    private Client client = (Client) Sessions.getCurrent().getAttribute("DATA");

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    private final LinkedList<QService> serviceList = ServiceList.getInstance().getServiceList();

    public LinkedList<QService> getServiceList() {
        return serviceList;
    }

    private QService selectedService;

    public QService getSelectedService() {
        return selectedService;
    }

    public void setSelectedService(QService selectedService) {
        selectedService.setInputCaption(selectedService.getInputCaption().replaceAll("<.*?>", ""));
        this.selectedService = selectedService;
    }

    @Command
    public void back() {
        Executions.sendRedirect("/");
    }

    @Command
    public void submit() {
        if (selectedService != null) {
            log.info("Selected service {};  ID={}; Caption: ", selectedService.getName(), selectedService.getId(), selectedService.getInputCaption());
            client.setService(selectedService);
            Executions.sendRedirect(selectedService.getInputRequired() ? "/inputServiceData.zul" : "/selectTime.zul");
        }
    }
}
