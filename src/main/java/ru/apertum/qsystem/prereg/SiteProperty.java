/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.prereg;

import java.io.Serializable;

/**
 * @author Evgeniy Egorov
 */
public class SiteProperty implements Serializable {

    String id;
    String title;
    String caption;
    String logoPath;
    String password;
    String serverAddr;
    String mailContentFile;
    int serverPort;
    String from = "prereg@qsystem.ru";
    String fromTitle = "QSystem";

    public SiteProperty(String id, String title, String caption, String logoPath, String password, String serverAddr, int serverPort, String mailContentFile) {
        this.id = id;
        this.title = title;
        this.caption = caption;
        this.logoPath = logoPath;
        this.password = password;
        this.serverAddr = serverAddr;
        this.serverPort = serverPort;
        this.mailContentFile = mailContentFile;
    }

    public String getMailContentFile() {
        return mailContentFile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getServerAddr() {
        return serverAddr;
    }

    public void setServerAddr(String serverAddr) {
        this.serverAddr = serverAddr;
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getFromTitle() {
        return fromTitle;
    }

    public void setFromTitle(String fromTitle) {
        this.fromTitle = fromTitle;
    }
}
