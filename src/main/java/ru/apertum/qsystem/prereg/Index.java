/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.prereg;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import ru.apertum.qsystem.common.Multilingual;

import java.util.ArrayList;
import java.util.Locale;

/**
 * @author Evgeniy Egorov
 */
public class Index {

    private Logger log = LogManager.getLogger(Index.class);

    public String l(String resName) {
        return Labels.getLabel(resName);
    }

    //*****************************************************
    //**** Multilingual
    //*****************************************************
    public ArrayList<Multilingual.Lng> getLangs() {
        return Multilingual.LANGS;
    }

    private Multilingual.Lng lang;

    public Multilingual.Lng getLang() {
        return lang;
    }

    public void setLang(Multilingual.Lng lang) {
        this.lang = lang;
    }

    @Command("changeLang")
    public void changeLang() {
        if (lang != null) {
            final org.zkoss.zk.ui.Session session = Sessions.getCurrent();
            final Locale prefer_locale = lang.code.length() > 2
                    ? new Locale(lang.code.substring(0, 2), lang.code.substring(3)) : new Locale(lang.code);
            session.setAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE, prefer_locale);
            Executions.sendRedirect(null);
        }
    }

    @Init
    public void init() {
        log.info("Index.init.");
        String lng = Executions.getCurrent().getParameter("locale");
        lang = new Multilingual().init(lng);

        String com = Executions.getCurrent().getParameter("com");
        com = com == null ? "" : (com + "_");
        log.trace("PROPS com = \"{}\"", com);
        final SiteProperty sp = new ru.apertum.qsystem.prereg.SiteProperty(com,
                System.getProperty(com + "QSYSPREREG_TITLE"),
                System.getProperty(com + "QSYSPREREG_CAPTION"),
                System.getProperty(com + "QSYSPREREG_LOGO"),
                System.getProperty(com + "QSYSPREREG_PASSWORD"),
                System.getProperty(com + "QSYSTEM_SERVER_ADDR"),
                Integer.parseInt(System.getProperty(com + "QSYSTEM_SERVER_PORT", "3128")),
                System.getProperty(com + "QSYSPREREG_MAIL_CONTENT"));
        sp.setFrom(System.getProperty(com + "QSYSPREREG_FROM"));
        sp.setFromTitle(System.getProperty(com + "QSYSPREREG_FROM_TITLE"));
        Sessions.getCurrent().setAttribute("PROPS", sp);
    }
}
