/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

/**
 * @author Evgeniy Egorov
 */
public class Multilingual {

    private Logger log = LogManager.getLogger(Multilingual.class);

    /**
     * @param lng типа мож дефолтный приехал
     * @return code
     */
    public Lng init(String lng) {
        russSymbolDateFormat = new DateFormatSymbols();
        russSymbolDateFormat.setMonths(RUSSIAN_MONAT);

        ukrSymbolDateFormat = new DateFormatSymbols();
        ukrSymbolDateFormat.setMonths(UKRAINIAN_MONAT);

        final DateFormatSymbols symbols = new DateFormatSymbols();
        switch (Locale.getDefault().toString().toLowerCase(Locale.US)) {
            case "ru_ru":
                symbols.setMonths(RUSSIAN_MONAT);
                break;
            case "uk_ua":
                symbols.setMonths(UKRAINIAN_MONAT);
                break;
            case "az_az":
                symbols.setMonths(AZERBAIJAN_MONAT);
                break;
            case "hy_am":
                symbols.setMonths(ARMENIAN_MONAT);
                break;
            default:
                log.trace("No cpec monats.");
        }

        formatForLabel = new SimpleDateFormat("dd MMMM HH.mm.ss", symbols);
        formatForLabel2 = new SimpleDateFormat("dd MMMM HH.mm:ss", symbols);
        formatForLabel2Short = new SimpleDateFormat("dd.MM HH.mm:ss", symbols);
        formatForPrint = new SimpleDateFormat("dd MMMM HH:mm", symbols);
        formatForPrintShort = new SimpleDateFormat("dd.MM HH:mm", symbols);
        formatDdMmmm = new SimpleDateFormat("dd MMMM", symbols);
        formatDdMmYyyyTime = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", symbols);
        formatDdMmmmYyyy = new SimpleDateFormat(DATE_FORMAT_FULL, symbols);
        formatDdMmYyyy = new SimpleDateFormat(DATE_FORMAT_FULL_SHORT, symbols);


        log.info("Init Multilingual = {}", lng);
        final org.zkoss.zk.ui.Session sess = Sessions.getCurrent();
        String ln;
        if (sess.getAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE) == null && Executions.getCurrent().getHeader("accept-language") != null && !Executions.getCurrent().getHeader("accept-language").isEmpty()) {
            ln = Executions.getCurrent().getHeader("accept-language").split(";")[0].replace("-", "_").split(",")[0];
        } else {
            ln = sess.getAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE) == null ? "hz_Ch"
                    : (((Locale) sess.getAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE)).getLanguage() + "_"
                            + ((Locale) sess.getAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE)).getCountry());
        }
        if (lng != null && !lng.isEmpty() && lng.matches("\\w\\w_\\w\\w")) {
            ln = lng;
        }
        Lng lang = ENG_LNG;
        boolean f = true;
        for (Lng lang1 : LANGS) {
            if (lang1.code.equalsIgnoreCase(ln)) {
                lang = lang1;
                f = false;
                break;
            }
        }

        if (f) {
            for (Lng lang1 : LANGS) {
                if (lang1.code.toLowerCase().startsWith(ln.split("_")[0].toLowerCase())) {
                    lang = lang1;
                    break;
                }
            }
        }
        final Locale prefer_locale = lang.code.length() > 2
                ? new Locale(lang.code.substring(0, 2), lang.code.substring(3)) : new Locale(lang.code);
        sess.setAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE, prefer_locale);
        if (!ln.equalsIgnoreCase(lang.code)) {
            Executions.sendRedirect(null);
        }
        log.info("Inited: {}", lang);
        return lang;
    }

    //*****************************************************
    //**** Multilingual
    //*****************************************************
    public static final Lng ENG_LNG = new Lng("English", "en_GB");
    public static final ArrayList<Lng> LANGS = new ArrayList<>(Arrays.asList(new Lng("Русский", "ru_RU"), ENG_LNG, new Lng("English", "en_US"), new Lng("Español", "es_ES"),
            new Lng("Deutsch", "de_DE"), new Lng("Português", "pt_PT"), new Lng("Français", "fr_FR"), new Lng("Italiano", "it_IT"), new Lng("čeština", "cs_CZ"),
            new Lng("Polski", "pl_PL"),
            new Lng("Slovenčina", "sk_SK"), new Lng("Român", "ro_RO"), new Lng("Cрпски", "sr_SP"), new Lng("Український", "uk_UA"), new Lng("Türk", "tr_TR"),
            new Lng("हिंदी", "hi_IN"),
            new Lng("العربية", "ar_EG"), new Lng("עברית", "iw_IL"), new Lng("Қазақ", "kk_KZ"), new Lng("Indonesia", "in_ID"), new Lng("Suomi", "fi_FI")));

    public static class Lng {

        final public String name;
        final public String code;

        public Lng(String name, String code) {
            this.name = name;
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public String getCode() {
            return code;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public static String getName(String code) {
        for (Lng lang1 : LANGS) {
            if (lang1.code.equalsIgnoreCase(code)) {
                return lang1.name;
            }
        }

        for (Lng lang1 : LANGS) {
            if (lang1.code.toLowerCase().startsWith(code.split("_")[0].toLowerCase())) {
                return lang1.name;
            }
        }
        return code;
    }

    public static String getCode(String name) {
        for (Lng lang1 : LANGS) {
            if (lang1.name.equalsIgnoreCase(name)) {
                return lang1.code;
            }
        }
        return name;
    }

    private static final String[] RUSSIAN_MONAT = {
            "Января",
            "Февраля",
            "Марта",
            "Апреля",
            "Мая",
            "Июня",
            "Июля",
            "Августа",
            "Сентября",
            "Октября",
            "Ноября",
            "Декабря"
    };

    public static String[] getRussianMonat() {
        return RUSSIAN_MONAT;
    }

    private static final String[] UKRAINIAN_MONAT = {
            "Січня",
            "Лютого",
            "Березня",
            "Квітня",
            "Травня",
            "Червня",
            "Липня",
            "Серпня",
            "Вересня",
            "Жовтня",
            "Листопада",
            "Грудня"
    };

    public static String[] getUkrainianMonat() {
        return UKRAINIAN_MONAT;
    }

    private static final String[] AZERBAIJAN_MONAT = {"Yanvar",
            "Fevral",
            "Mart",
            "Aprel",
            "May",
            "Iyun",
            "Iyul",
            "Avqust",
            "Sentyabr",
            "Oktyabr",
            "Noyabr",
            "Dekabr"};

    public static String[] getAzerbaijanMonat() {
        return AZERBAIJAN_MONAT;
    }

    private static final String[] ARMENIAN_MONAT = {"Հունվարի",
            "Փետրվարի",
            "Մարտի",
            "Ապրիլի",
            "Մայիսի",
            "Հունիսի",
            "Հուլիսի",
            "Օգոստոսի",
            "Սեպտեմբերի",
            "Հոկտեմբերի",
            "Նոեմբերի",
            "Դեկտեմբերի"};

    public static String[] getArmenianMonat() {
        return ARMENIAN_MONAT;
    }

    /**
     * Формат даты без времени, с годом и месяц прописью.
     */
    public static final String DATE_FORMAT_FULL = "dd MMMM yyyy";
    public static final String DATE_FORMAT_FULL_SHORT = "dd.MM.yyyy";
    /**
     * Форматы дат 2009 январь 26 16:10:41.
     */
    public SimpleDateFormat formatForLabel;
    public SimpleDateFormat formatForLabel2;
    public SimpleDateFormat formatForLabel2Short;
    public SimpleDateFormat formatForPrint;
    public SimpleDateFormat formatForPrintShort;
    public SimpleDateFormat formatDdMmmm;
    public SimpleDateFormat formatDdMmYyyyTime;
    public SimpleDateFormat formatDdMmmmYyyy;
    public SimpleDateFormat formatDdMmYyyy;

    private DateFormatSymbols russSymbolDateFormat;
    private DateFormatSymbols ukrSymbolDateFormat;

    public DateFormatSymbols getRussSymbolDateFormat() {
        return russSymbolDateFormat;
    }

    public DateFormatSymbols getUkrSymbolDateFormat() {
        return ukrSymbolDateFormat;
    }
}
